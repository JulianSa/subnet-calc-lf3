from math import ceil, log2

def validate_user_address(user_address):
    parts = user_address.split('/')
    
    if len(parts) != 2:
        return False
    
    network_address, cidr = parts
    
    octets = network_address.split('.')
    
    if len(octets) != 4:
        return False 
    try:
        for octet in octets:
            if not 0 <= int(octet) <= 255:
                return False
        
        cidr_int = int(cidr)
        if not 0 <= cidr_int <= 32:
            return False
        
        return True
    except ValueError:
        return False

def divide_subnet(network_subnet):
    subnet_parts = [network_subnet[i:i+8] for i in range(0, 32, 8)]
    subnet_decimal = '.'.join(str(int(part, 2)) for part in subnet_parts)
    return subnet_decimal

def divide_address(user_address):
    network_address, cidr = user_address.split('/')
    network_subnet = '1' * int(cidr) + '0' * (32 - int(cidr))
    subnet_decimal = divide_subnet(network_subnet)
    return subnet_decimal, network_address, cidr

def calc_base_address(network_address, subnet_decimal):
    ip_octets = [int(octet) for octet in network_address.split('.')]
    mask_octets = [int(octet) for octet in subnet_decimal.split('.')]
    network_octets = [ip_octets[i] & mask_octets[i] for i in range(4)]
    return '.'.join(map(str, network_octets))

def calc_new_cidr_and_mask(cidr, num_subnets):
    subnet_log = ceil(log2(num_subnets))
    new_cidr = int(cidr) + subnet_log
    
    new_subnet_bin = '1' * new_cidr + '0' * (32 - new_cidr)
    new_subnet_parts = [new_subnet_bin[i:i+8] for i in range(0, 32, 8)]
    new_subnet_decimal = '.'.join(str(int(part, 2)) for part in new_subnet_parts)
    
    return new_cidr, new_subnet_bin, new_subnet_decimal

def calc_hosts(new_subnet_bin):
    host_bits = new_subnet_bin.count('0')
    total_hosts = 2 ** host_bits
    usable_hosts = total_hosts - 2 
    
    return total_hosts, usable_hosts

def calc_ip2int(ip):
    a, b, c, d = [int(i) for i in ip.split('.')]
    return (a << 24) + (b << 16) + (c << 8) + d

def calc_int2ip(i):
    return '%d.%d.%d.%d' % (
        (i & 0b11111111000000000000000000000000) >> 24,
        (i & 0b00000000111111110000000000000000) >> 16,
        (i & 0b00000000000000001111111100000000) >> 8,
        i & 0b00000000000000000000000011111111
        )

repeat = True
while repeat:
    user_address = input("Geben Sie die IP-Adresse in CIDR-Notation ein (z. B. 10.10.128.0/24): ")
    if not validate_user_address(user_address):
        print("Ungültige Eingabe. Bitte geben Sie eine gültige IPv4-Netzwerkadresse in CIDR-Notation ein.")
        continue
    
    subnet_decimal, network_address, cidr = divide_address(user_address)

    base_address = calc_base_address(network_address, subnet_decimal)

    num_subnets = int(input("Geben Sie die Anzahl der gewünschten Subnetze ein (1-32): ")) 

    new_cidr, new_subnet_bin, new_subnet_decimal = calc_new_cidr_and_mask(cidr, num_subnets)

    total_hosts, usable_hosts = calc_hosts(new_subnet_bin)
    if usable_hosts < 1:
        print('Ungültig, versuchen Sie es erneut')
        continue
    
    ip_int = calc_ip2int(base_address)
    mask_int = calc_ip2int(new_subnet_decimal)
    print(f'Ihre Basisnetzwerk ist {base_address}/{cidr}, mit Subnetzmaske {subnet_decimal}')
    print(f'Für {num_subnets} Subnetze haben Sie {usable_hosts} verwendbare Hosts für jedes Subnetz')
    print(f'Ihre neue Subnetzmaske für jedes Subnetz ist {new_subnet_decimal}')
    for i in range(num_subnets):
        print('Basis Netz:', calc_int2ip(ip_int)+'/'+str(new_cidr), 'Erste IP:', calc_int2ip(ip_int + 1), 'Letzte IP:', calc_int2ip(ip_int | ~mask_int - 1))
        ip_end = calc_int2ip(ip_int | ~mask_int)
        ip_int = calc_ip2int(ip_end) + 1
        
    repeat_calculation = input("Möchten Sie eine weitere Berechnung durchführen? (Ja/Nein): ").lower()
    if repeat_calculation != "ja":
        repeat = False